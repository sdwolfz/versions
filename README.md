# Spyglass

A version scraper and checker.

## Prerequisites

| Name          | Link                                    |
|---------------|-----------------------------------------|
| `docker`      | https://docs.docker.com/engine/install/ |
| `docker-here` | https://gitlab.com/sdwolfz/docker-here  |
| `make`        | https://www.gnu.org/software/make/      |

## Build

```sh
make build
```

## Run

```sh
make shell
```

## Binary

```sh
make compile
```

## Release

```sh
make run
```

## Configuration

Create a `spyglass.yml` file with:

```yml
notify:
  - command: notify-send -u critical '%{title}' '%{body}'

scan:
  - namespace: Docker Images
    groups:
      - name: Alpine
        latest:
          url: https://www.alpinelinux.org/downloads/
          selector: "#content p strong"
        current:
          - name: docker-images
            items:
              - id: ansible-lint
                url: https://gitlab.com/sdwolfz/docker-images/-/raw/master/ansible-lint/Dockerfile
                regex: ^FROM alpine:(.*?)$
              - id: eclint
                url: https://gitlab.com/sdwolfz/docker-images/-/raw/master/eclint/Dockerfile
                regex: ^FROM alpine:(.*?)$
```

Run `spyglass` in the same directory as the config file, it will output:

```
Docker Images
  Alpine
    docker-images
      eclint
        Up to date!
      shellcheck
        Latest: 3.12.1 | Current: 3.12.0
```

And you will see a system notification create with the `notify-send` command.

## License

The rest of the code is covered by the BSD 3-clause License, see
[LICENSE](LICENSE) for more details.
