require "http/client"
require "myhtml"

class Scanner
  @config : Array(YAML::Any)

  def initialize(config : YAML::Any) : Nil
    @config  = config.as_a
    @channel = Channel(String).new
  end

  def call : Nil
    handle_namespaces(@config)
  end

  #-----------------------------------------------------------------------------
  # Private
  #-----------------------------------------------------------------------------

  private def handle_namespaces(config : Array(YAML::Any)) : Nil
    config.each do |element|
      namespace = element.as_h["namespace"].as_s
      groups    = element.as_h["groups"].as_a

      display_namespace(namespace)

      handle_groups(groups)
    end
  end

  private def handle_groups(groups : Array(YAML::Any)) : Nil
    groups.each do |element|
      name    = element.as_h["name"].as_s
      latest  = element.as_h["latest"].as_h
      current = element.as_h["current"].as_a

      display_group(name)

      version = handle_latest(latest)
      handle_current(current, version)
    end
  end

  private def handle_latest(latest : Hash(YAML::Any, YAML::Any)) : String
    extract_latest(latest)
  end

  private def handle_current(current : Array(YAML::Any), latest : String) : Nil
    current.each do |element|
      name  = element.as_h["name"].as_s
      items = element.as_h["items"].as_a

      display_list(name)

      version = handle_current_items(items, latest)
    end
  end

  private def handle_current_items(items : Array(YAML::Any), latest) : Nil
    items.each do |element|
      spawn do
        id      = element.as_h["id"].as_s
        version = extract_current(element.as_h)
        result  = display_results(id, version, latest)

        @channel.send(result)
      rescue e
        puts "ERROR: #{e.message}"
        puts "For: #{element}"

        @channel.send("")
      end
    end

    items.size.times do
      result = @channel.receive

      puts result
    end
  end

  # ----------------------------------------------------------------------------
  # Extraction

  private def extract_latest(config : Hash(YAML::Any, YAML::Any)) : String
    url  = config["url"].as_s
    body = page_body(url)

    selector = config["selector"].as_s
    extract_dom_text(body, selector)
  end

  private def extract_current(config : Hash(YAML::Any, YAML::Any)) : String
    url  = config["url"].as_s
    body = page_body(url)

    if regex_string = config["regex"]?
      regex = Regex.new(regex_string.as_s, Regex::Options::MULTILINE)

      extract_regex_text(body, regex)
    elsif selector_string = config["selector"]?
      selector = selector_string.as_s

      extract_dom_text(body, selector)
    else
      raise "Need either `regex` or `selector` configured!"
    end
  end

  # ----------------------------------------------------------------------------
  # Download

  private def page_body(url : String) : String
    response = HTTP::Client.get(url)

    if response.status_code == 302
      url = response.headers["location"]
      response = HTTP::Client.get(url)
    end

    response.body
  end

  # ----------------------------------------------------------------------------
  # Transform

  private def extract_dom_text(body : String, selector : String) : String
    html = Myhtml::Parser.new(body)

    html.css(selector).map(&.inner_text).to_a.first.strip
  end

  private def extract_regex_text(body : String, regex : Regex) : String
    match = regex.match(body)

    if match
      return match[1]
    end

    return ""
  end

  # ----------------------------------------------------------------------------
  # Display

  private def display_namespace(namespace : String) : Nil
    puts namespace
  end

  private def display_group(name : String) : Nil
    puts "  #{name}"
  end

  private def display_list(name : String) : Nil
    puts "    #{name}"
  end

  private def display_results(id : String, current : String, latest : String) : String
    result = "      #{id}\n        "

    if current == latest
      result += "Up to date!\n"
    else
      result += "Latest: #{latest} | Current: #{current}\n"
    end

    result
  end
end
