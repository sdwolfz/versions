class Notifier
  @config : Array(YAML::Any)

  def initialize(config : YAML::Any) : Nil
    @config = config.as_a
  end

  def call(title : String, body : String) : Nil
    @config.each do |element|
      command = element.as_h["command"].as_s
      result  = command % {title: title, body: body}

      Process.run(result, shell: true)
    end
  end
end
