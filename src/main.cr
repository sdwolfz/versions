require "yaml"

require "./scanner"
require "./notifier"

config = YAML.parse(File.read("./spyglass.yml"))

scanner = Scanner.new(config["scan"])
scanner.call

notifier = Notifier.new(config["notify"])
title    = "Spyglass"
body     = "Finished"
notifier.call(title: title, body: body)
